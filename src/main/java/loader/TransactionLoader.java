package loader;

import accounts.IAccountHandler;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import transaction_messages.TransactionMessage;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

public class TransactionLoader{
    private IAccountHandler accountHandler;
    private LinkedList<TransactionMessage> loadedMessages;
    private int loadDelay = 5000;
    public TransactionLoader(IAccountHandler accountHandler)
    {
        this.accountHandler = accountHandler;
        loadedMessages = new LinkedList<>();
    }
    public void LoadTrx()
    {
        ExecutorService executorService = Executors.newScheduledThreadPool(3);
        final File loaderDir = new File("transactions");
        if (loaderDir.mkdirs())
        {
            System.out.println(loaderDir.getAbsolutePath()+"folder is created");
        }
        System.out.println("Waiting for messages in "+loaderDir.getAbsolutePath());
        ((ScheduledExecutorService) executorService).scheduleWithFixedDelay(() -> {
         File[] files = loaderDir.listFiles(pathname -> pathname.getPath().endsWith(".json"));
            if (files != null && files.length>0) {
                for (File file : files) {
                    try {
                        ObjectMapper jsonParser = new ObjectMapper();
                     TransactionMessage trxMsg=  jsonParser.readValue(file,TransactionMessage.class);
                     loadedMessages.add(trxMsg);
                     accountHandler.handleTransaction(trxMsg,loadedMessages.size()%10==0);
                     if (file.delete())
                     {
                         System.out.println(loadedMessages.size()+" transactions sre loaded");
                     }


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }else
            {
                System.out.format("No message to load, try again in %d ms",loadDelay/1000).println();
            }


        },0,loadDelay,TimeUnit.MILLISECONDS);
    }
}
