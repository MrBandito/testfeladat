package accounts;

import transaction_messages.TransactionMessage;

public interface IAccountHandler
{
    void handleTransaction(TransactionMessage trxMsg,boolean generateReport);
}
