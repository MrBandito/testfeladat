package accounts;

public enum TransactionType {
    Debit,
    Credit
}
