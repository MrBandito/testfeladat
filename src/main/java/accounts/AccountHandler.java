package accounts;

import transaction_messages.TransactionMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

public class AccountHandler implements IAccountHandler{
    private ConcurrentHashMap<String,LinkedList<AccountHistory>> accountHistory = new ConcurrentHashMap<>(2);
    private ConcurrentHashMap<String,Account> accounts = new ConcurrentHashMap<>(2);
    private Account currentAccount;
    public AccountHandler()
    {
        accounts.put("11111111-22222222",new Account("11111111-22222222", "HUF", 150000));
        accounts.put("22222222-33333333",new Account("22222222-33333333","USD",1230));
    }

    public synchronized void handleTransaction(TransactionMessage trxMsg,boolean generateReport) {
        if (!accounts.containsKey(trxMsg.getAccount()))
        {
           System.out.format("%s not found",trxMsg.getAccount()).println();
        }else
        {
           currentAccount= accounts.get(trxMsg.getAccount());
           if (currentAccount.getCurrency().equals(trxMsg.getCurrency()))
           {
             updateBalance(trxMsg.getAccount(),trxMsg.getCurrency(),currentAccount.getBalance()+trxMsg.getValue());
           }else
           {
               updateBalance(trxMsg.getAccount(),trxMsg.getCurrency(),currentAccount.getBalance()+(trxMsg.getValue()*trxMsg.getCurrencyExchange()));
           }
        }
        if(generateReport)
        {
            accountHistory.forEach((s, accounts) -> {
                accounts.forEach(accountHistory -> {
                    System.out.format("%s - %s",s,accountHistory.toString()).println();
                });
            });
        }
    }

    private void updateBalance(String account,String currency, float newValue)
    {
        LinkedList<AccountHistory> prevAccountState = accountHistory.getOrDefault(account,new LinkedList<>());
        LinkedList<AccountHistory> newAccountState = new LinkedList<>(prevAccountState);
        newAccountState.add(new AccountHistory(currentAccount.getBalance(),
                newValue,String.format("%s - %s",currency,currentAccount.getCurrency()),
                newValue < currentAccount.getBalance()? TransactionType.Debit:TransactionType.Credit));
        accountHistory.put(account,newAccountState);
    }
}
