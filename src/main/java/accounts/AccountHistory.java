package accounts;

public class AccountHistory {
    private float oldBalance;
    private float newBalance;
    private String currencyConversion;
    private TransactionType type;

    public AccountHistory(float oldBalance, float newBalance, String currencyConversion, TransactionType type) {
        this.oldBalance = oldBalance;
        this.newBalance = newBalance;
        this.currencyConversion = currencyConversion;
        this.type = type;
    }

    @Override
    public String toString() {
        return String.format("old balance: %2g, new balance: %2g, currency from to: %s, type: %s",oldBalance,newBalance,currencyConversion,type);
    }
}
