package transaction_messages;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TransactionMessage {
    @JsonProperty("Account")
    private String account;

    @JsonProperty("Currency")
    private String currency;

    @JsonProperty("Value")
    private float value;

    @JsonProperty("CurrencyExchange")
    private float currencyExchange;

    public TransactionMessage()
    {

    }
    public TransactionMessage(String account, String currency, float value, float currencyExchange) {
        this.account = account;
        this.currency = currency;
        this.value = value;
        this.currencyExchange = currencyExchange;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public float getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public float getCurrencyExchange() {
        return currencyExchange;
    }

    public void setCurrencyExchange(int currencyExchange) {
        this.currencyExchange = currencyExchange;
    }
}
