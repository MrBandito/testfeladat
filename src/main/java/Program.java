import accounts.AccountHandler;
import loader.TransactionLoader;

public class Program {
    public static void  main(String[] args)
    {
        AccountHandler accountHandler = new AccountHandler();
        TransactionLoader trxLoader =  new TransactionLoader(accountHandler);
        trxLoader.LoadTrx();
    }
}
